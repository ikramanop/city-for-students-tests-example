module gitlab.com/ikramanop/city-for-students-tests-example

go 1.17

require (
	github.com/fatih/color v1.7.0 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/lamoda/gonkey v1.14.0 // indirect
	github.com/lib/pq v1.3.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/tidwall/gjson v1.13.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
