package tests

import "testing"

func TestExample(t *testing.T)  {
	TestRunner {
		cases: []string{"cases/examples"},
		mocks: []string{"eventsBackend"},
	}.Run(t)
}