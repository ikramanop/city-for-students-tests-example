package tests
 
import (
    "database/sql"
    "errors"
    "testing"
	"os"

    "github.com/lamoda/gonkey/fixtures"
    "github.com/lamoda/gonkey/mocks"
    "github.com/lamoda/gonkey/runner"
)
 
const eventsBackendHost = ""
const postgresDbRwDsn = ""

type TestRunner struct {
    mocks            []string
    cases         	 []string
    skip             bool
}

func (r TestRunner) Run(t *testing.T) {
    if r.skip {
        t.Skip("skipped")
    }

    var m *mocks.Mocks
    if len(r.mocks) > 0 {
        m = mocks.NewNop(r.mocks...)
        if err := m.Start(); err != nil {
            t.Fatal(err)
        }
        defer m.Shutdown()

        for _, val := range r.mocks {
            switch val {
            case "eventsBackend":
                os.Setenv(eventsBackendHost, m.Service("eventsBackend").ServerAddr())
            default:
                t.Fatal(errors.New("unknown mock name"))
            }
        }
    }

    var db *sql.DB
    db, err := sql.Open("postgres", os.Getenv(postgresDbRwDsn))
    if err != nil {
        t.Fatal("Can't connect to db over DSN",  os.Getenv(postgresDbRwDsn), err)
    }
    defer db.Close()

    for _, dir := range r.cases {
        runner.RunWithTesting(t, &runner.RunWithTestingParams{
            TestsDir:    dir,
            DB:          db,
            DbType:      fixtures.Postgres,
            FixturesDir: "./fixtures",
            Mocks:       m,
        })
    }
}